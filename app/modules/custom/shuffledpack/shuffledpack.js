/**
 * @file
 * Custom code for Shuffled Pack.
 */

/**
 * Implements hook_install().
 */
function shuffledpack_install() {
  try {
    var css = drupalgap_get_path('module', 'shuffledpack') + '/shuffledpack.css';
    drupalgap_add_css(css);
    shuffledpack_server_status_render();
    setInterval(shuffledpack_init_ping, 61000);
  }
  catch (error) {consoel.log('shuffledpack_install - ' + error);}
}

/**
 * Implements hook_menu().
 */
function shuffledpack_menu() {
  try {
    var items = {};
    items['node/%/delete'] = {
      'title': t('Delete'),
      'page_callback': 'entity_page_edit',
      'pageshow': 'entity_page_edit_pageshow',
      'page_arguments': ['node_delete_form', 'node', 1],
      'weight': 0,
      'type': 'MENU_LOCAL_TASK',
      'access_callback': 'node_access',
      'access_arguments': [1],
      options: {reloadPage: true}
    }

    items['gin-games'] = {
      title: t('Gin Games'),
      page_callback: 'shuffledpack_gin_games_page',
    };
    items['user/%/gin-games'] = {
      title: t('Gin Games'),
      title_callback: 'shuffledpack_user_games_page_title',
      title_arguments: ['gin', 1],
      page_callback: 'shuffledpack_user_games_page',
      page_arguments: ['gin', 1],
      type: 'MENU_LOCAL_TASK',
    };
    items['gin-statistics'] = {
      title: t('Gin Statistics'),
      page_callback: 'shuffledpack_statistics_page',
      page_arguments: ['gin'],
      pageshow: 'shufflepack_pageshow',
    };
    items['user/%/gin-statistics'] = {
      title: t('Gin Statistics'),
      title_callback: 'shuffledpack_user_statistics_page_title',
      title_arguments: ['gin', 1],
      page_callback: 'shuffledpack_statistics_page',
      page_arguments: ['gin', 1],
      pageshow: 'shufflepack_pageshow',
      type: 'MENU_LOCAL_TASK',
    }
    items['spades-games'] = {
      title: t('Spades Games'),
      page_callback: 'shuffledpack_spades_games_page',
    };
    items['user/%/spades-games'] = {
      title: t('Spades Games'),
      title_callback: 'shuffledpack_user_games_page_title',
      title_arguments: ['spades', 1],
      page_callback: 'shuffledpack_user_games_page',
      page_arguments: ['spades', 1],
      type: 'MENU_LOCAL_TASK',
    };
    items['spades-statistics'] = {
      title: t('Spades Statistics'),
      page_callback: 'shuffledpack_statistics_page',
      page_arguments: ['spades'],
      pageshow: 'shufflepack_pageshow',
    };
    items['user/%/spades-statistics'] = {
      title: t('Spades Statistics'),
      title_callback: 'shuffledpack_user_statistics_page_title',
      title_arguments: ['spades', 1],
      page_callback: 'shuffledpack_statistics_page',
      page_arguments: ['spades', 1],
      pageshow: 'shufflepack_pageshow',
      type: 'MENU_LOCAL_TASK',
    }
    return items;
  }
  catch (error) {console.log("shuffledpack_menu - " + error);}
}

/**
 * Title callback for the user games pages.
 */
function shuffledpack_user_games_page_title(callback, game_type, uid) {
  try {
    user_load(uid, {
      success: function(account) {
        callback.call(null, account.name + "'s " + game_type.charAt(0).toUpperCase() + game_type.slice(1) + " games");
      }
    });
  }
  catch (error) {console.log('shuffledpack_user_games_page_title - ' + error);}
}

/**
 * Title callback for the user statistics pages.
 */
function shuffledpack_user_statistics_page_title(callback, game_type, uid) {
  try {
    user_load(uid, {
      success: function(account) {
        callback.call(null, account.name + "'s " + game_type.charAt(0).toUpperCase() + game_type.slice(1) + " Statistics");
      }
    });
  }
  catch (error) {console.log('shuffledpack_user_statistics_page_title - ' + error);}
}

/**
 * Page callback for gin-games.json.
 */
function shuffledpack_gin_games_page() {
  try {
    var content = {};
    content['gin_games'] = {
      theme: 'view',
      format: 'ul',
      path: 'gin-games.json',
      row_callback: 'shuffledpack_gin_games_list_row',
      empty_callback: 'shuffledpack_gin_games_list_empty',
      attributes: {
        id: 'shuffledpack-gin-games-list-view',
      },
    };
    return content;
  }
  catch (error) {console.log("shuffledpack_gin_games_page - " + error);}
}

/**
 * Page callback for spades-games.json.
 */
function shuffledpack_spades_games_page() {
  try {
    var content = {};
    content['top_statistics'] = {
      theme: 'collapsible',
      header: t("Top players"),
      attributes: {
        'data-collapsed': 'true',
      },
      content: '<div id="spades-games-top-statistics"></div>',
    }
    shuffledpack_top_statistics_load({
      success: function (result) {
        var content = shuffledpack_spades_top_statistics_render(result);
        $("#spades-games-top-statistics").html(content).trigger("create");
      },
      error: function(xhr, status, msg) {
        if (drupalgap.page.options.error) { drupalgap.page.options.error(xhr, status, msg); }
      },
    });
    content['spades-games'] = {
      theme: 'view',
      format: 'ul',
      path: 'spades-games.json',
      row_callback: 'shuffledpack_spades_games_list_row',
      empty_callback: 'shuffledpack_spades_games_list_empty',
      attributes: {
        id: 'shuffledpack-spades-games-list-view',
      },
    };
    return content;
  }
  catch (error) {console.log("shuffledpack_spades_games_page - " + error);}
}

/**
 * Page callback for user/%/[game_type]-games.json.
 *
 * @param game_type
 *   The type of game to show.
 * @param uid
 *   The user ID of the person whose games should be shown.
 */
function shuffledpack_user_games_page(game_type, uid) {
  try {
    var content = {};
    content[game_type + '_games'] = {
      theme: 'view',
      format: 'ul',
      path: 'user/' + uid + '/' + game_type + '-games.json',
      row_callback: 'shuffledpack_' + game_type + '_games_list_row',
      empty_callback: 'shuffledpack_' + game_type + '_games_list_empty',
      attributes: {
        id: 'shuffledpack-' + game_type + '-games-' + uid + '-list-view',
      },
    };
    return content;
  }
  catch (error) {console.log("shuffledpack_user_games_page - " + error);}
}

/**
 * Row callback for views of Gin games.
 */
function shuffledpack_gin_games_list_row(view, row, variables) {
  try {
    var player_names = row.player_name.split(",");
    var player_uids = row.Players.split(",");
    var data = [
      [row.Date],
      [l(player_names[0].trim(), 'user/' + player_uids[0].trim() + '/gin-games') + ', ' + l(player_names[1], 'user/' + player_uids[1].trim() + '/gin-games')],
      [t('Won by') + ' ' + l(row.winner_name, 'user/' + row.Winner + '/gin-games')],
      [t('Score') + ': ' + row.Score],
      [row.Comment],
      [row.Verified]
    ];
    if ($.inArray(Drupal.user.uid, player_uids) >=0 && row.Verified == "Not verified") {
      data.push([l(t("Delete"), 'node/' + row.nid + '/delete')]);
    }
    var table = {
      header: [],
      rows: data,
      attributes: {},
    };
    return theme('table', table);
  }
  catch (error) {
    console.log('shuffledpack_gin_games_list_row - ' + error);
    return t("There was an error rendering this row.  Please report this.  The error was") + " <code>" + error + '</code>';
  }
}

/**
 * Row callback for views of Spades games.
 */
function shuffledpack_spades_games_list_row(view, row, variables) {
  try {
    var player_names = row.player_name.split(",");
    var player_uids = row.Players.split(",");
    var winner_names = row.winner_name.split(",");
    var winner_uids = row.Winners.split(",");
    var data = [
      [row.Date],
      [l(player_names[0].trim(), 'user/' + player_uids[0].trim() + '/spades-games')
        + ' ' + t('and') + ' '
        + l(player_names[2], 'user/' + player_uids[2].trim() + '/spades-games')
        + '<br>' + t('vs.') + '<br>'
        + l(player_names[1].trim(), 'user/' + player_uids[1].trim() + '/spades-games')
        + ' ' + t('and') + ' '
        + l(player_names[3].trim(), 'user/' + player_uids[3].trim() + '/spades-games')
      ],
      [t('Won by') + ' '
        + l(winner_names[0], 'user/' + winner_uids[0].trim() + '/spades-games')
        + ' ' + t('and') + ' '
        + l(winner_names[1], 'user/' + winner_uids[1].trim() + '/spades-games')
      ]
    ];
    /// @todo Add delete button if appropriate.  Get verifications working for
    /// Spades games first.
    var table = {
      header: [],
      rows: data,
      attributes: {},
    };
    return theme('table', table);
  }
  catch (error) {
    console.log('shuffledpack_spades_games_list_row - ' + error);
    return t("There was an error rendering this row.  Please report this.  The error was") + " <code>" + error + '</code>';
  }
}

/**
 * Empty callback for shuffledpack_gin_games_page().
 */
function shuffledpack_gin_games_list_empty(view, variables) {
  try {
    return t('Sorry, no Gin games were found.');
  }
  catch (error) {console.log('shuffledpack_gin_games_list_empty - ' + error);}
}

/**
 * Empty callback for shuffledpack_spades_games_page().
 */
function shuffledpack_spades_games_list_empty(view, variables) {
  try {
    return t('Sorry, no Spades games were found.');
  }
  catch (error) {console.log('shuffledpack_spades_games_list_empty - ' + error);}
}

/**
 * Page callback for statistics.
 */
function shuffledpack_statistics_page(game_type, uid) {
  try {
    var content = {
      container: {
        markup: '<div id="' + shuffledpack_statistics_id(game_type, uid) + '"></div>',
      }
    };
    return content;
  }
  catch (error) {console.log('shuffledpack_statistics_page - ' + error);}
}

/**
 * Pageshow callback for statistics.
 *
 * @param string game_type
 *   The type of game of interest.  For example, "gin" or "spades".
 */
function shufflepack_pageshow(game_type, uid) {
  try {
    data = {};
    if (typeof uid != 'undefined') {
      data['uid'] = uid;
    }
    shuffledpack_statistics_load({
      data: JSON.stringify(data),
      success: function (result) {
        var content = window['shuffledpack_' + game_type + '_statistics_render'](result);
        $('#' + shuffledpack_statistics_id(game_type, uid)).html(content).trigger('create');
        try {
          $("table.sortable").tablesorter();
        }
        catch (error) {
          console.log("shuffledpack_pageshow - tablesorter failed.  Is the library included in the project?  The message was: " + error);
        }
      },
      error: function(xhr, status, msg) {
        if (drupalgap.page.options.error) { drupalgap.page.options.error(xhr, status, msg); }
      },
    }, game_type);
  }
  catch (error) {console.log('shuffledpack_pageshow - ' + error);}
}

/**
 * Gets an ID of a statistics page.
 */
function shuffledpack_statistics_id(game_type, uid) {
  try {
    if (typeof uid == 'undefined') {
      var id = 'shuffledpack_statistics_' + game_type;
    }
    else {
      var id = 'shuffledpack_statistics_' + game_type + '-' + uid;
    }
    return id;
  }
  catch (error) {console.log("shuffledpack_statistics_id - " + error);}
}

/**
 * Renders Gin statistics.
 */
function shuffledpack_gin_statistics_render(data) {
  try {
    var header = [
      {data: t('Name')},
      {data: t('Games')},
      {data: t('Wins')},
      {data: t('Losses')},
      {data: t('Percentage')},
      {data: t('Score')}
    ];
    var rows = [];
    for (var i in data) {
      if (is_int(i) && parseInt(data[i].games)) {
        rows.push([
          l(data[i].name, 'user/' + i + '/gin-statistics'),
          data[i].games,
          data[i].wins,
          data[i].losses,
          parseFloat(data[i].percentage).toFixed(2),
          data[i].score
        ]);
      }
    }
    if (typeof data.aggregate !== 'undefined') {
      rows.push([
        t('Total'),
        data.aggregate.games,
        data.aggregate.wins,
        data.aggregate.losses,
        parseFloat(data.aggregate.percentage).toFixed(2),
        data.aggregate.score
      ]);
    }
    var table = {
      header: header,
      rows: rows,
      attributes: {},
    };
    if (rows.length > 1) {
      table.attributes.class = "sortable";
    }
    return theme('table', table);
  }
  catch (error) {console.log('shuffledpack_gin_statistics_render - ' + error);}
}

/**
 * Renders Spades statistics.
 */
function shuffledpack_spades_statistics_render(data) {
  try {
    if (typeof data.uid != 'undefined') {
      // This is a user page.
      return shuffledpack_spades_user_statistics_render(data);
    }
    var content = [];
    content.push(shuffledpack_spades_top_statistics_render(data.top));
    // Individuals.
    var header = [
      {data: t("Name")},
      {data: t("Games")},
      {data: t("Wins")},
      {data: t("Losses")},
      {data: t("Percentage")}
    ];
    var rows = [];
    for (var i in data.individuals) {
      rows.push([
        l(data.individuals[i].name, 'user/' + i + '/spades-statistics'),
        data.individuals[i].games,
        data.individuals[i].wins,
        data.individuals[i].losses,
        parseFloat(data.individuals[i].percentage).toFixed(2)
      ]);
    }
    var table = {
      header: header,
      rows: rows,
      attributes: {},
    };
    if (rows.length > 1) {
      table.attributes.class = "sortable";
    }
    content.push(theme('table', table));
    content.push("<br>");

    // Partnerships.
    header[0] = {data: t("Partnership")};
    rows = [];
    for (var i in data.partnerships) {
      var uids = Object.keys(data.partnerships[i].name);
      rows.push([
          l(data.partnerships[i].name[uids[0]].name, 'user/' + uids[0] + '/spades-statistics')
        + '<br>and<br>'
        + l(data.partnerships[i].name[uids[1]].name, 'user/' + uids[1] + '/spades-statistics'),
        data.partnerships[i].games,
        data.partnerships[i].wins,
        data.partnerships[i].losses,
        parseFloat(data.partnerships[i].percentage).toFixed(2)
      ]);
      rows.push(["<br>"]);
    }
    table = {
      header: header,
      rows: rows,
      attributes: {},
    };
    if (rows.length > 1) {
      table.attributes.class = "sortable";
    }
    content.push(theme('table', table));
    return content;
  }
  catch (error) {console.log('shuffledpack_spades_statistics_render - ' + error);}
}

/**
 * Renders Spades statistics for a user.
 */
function shuffledpack_spades_user_statistics_render(data) {
  try {
    console.log(data);
    var content = [];
    // Self.
    content.push(shuffledpack_build_table_with_aggregate(data, " "));
    for (var i in data.partners) {
      content.push(theme('markup', {
        markup: '<h3>' + t('With') + ' ' + shuffledpack_render_name(data.partner_names[i], i, 'spades', 'statistics') + '</h3>',
      }));
      content.push(shuffledpack_build_table_with_aggregate(data.partners[i]));
    }
    return content;
  }
  catch (error) {console.log('shuffledpack_spades_user_statistics_render - ' + error);}
}

/**
 * Builds a table containing a user's or partnerships's aggregate statistics.
 *
 * @param data
 *   The data to work with.
 * @param header_first_column
 *   Override the first column header.  The default is t("Against").
 */
function shuffledpack_build_table_with_aggregate(data, header_first_column) {
  try {
    var header = [
      {data: t("Against")},
      {data: t("Games")},
      {data: t("Wins")},
      {data: t("Losses")},
      {data: t("Percentage")}
    ];
    var rows = [];
    if (typeof header_first_column != 'undefined') {
      header[0] = {data: header_first_column};
    }
    for (var i in data) {
      if (is_int(i)) {
        rows.push([
          shuffledpack_render_name(data[i].name, null, "spades", "statistics"),
          data[i].games,
          data[i].wins,
          data[i].losses,
          parseFloat(data[i].percentage).toFixed(2)
        ]);
      }
    }
    if (rows.length != 1) {
      rows.push([
        t("Total"),
        data.aggregate.games,
        data.aggregate.wins,
        data.aggregate.losses,
        parseFloat(data.aggregate.percentage).toFixed(2)
      ]);
    }
    var table = {
      header: header,
      rows: rows,
      attributes: {},
    };
    if (rows.length > 1) {
      table.attributes.class = "sortable";
    }
    return theme('table', table);
  }
  catch (error) {console.log("shuffledpack_build_table_with_aggregate - " + error);}
}

/**
 * Renders a name according to the type given.
 *
 * @param name
 *   An object containing uid and mame or string name with a uid as the
 *   second argument.
 * @param uid
 *   Provide the user ID associated with the name if name is a string.
 * @param game_type
 *   "spades" or "gin".
 * @param page_type
 *   "games" or "statistics".
 *
 * @return string
 *   A formatted username or two.
 */
function shuffledpack_render_name(name, uid, game_type, page_type) {
  try {
    if (typeof name == 'object') {
      var rendered_names = [];
      for (var uid in name) {
        if (is_int(uid)) {
          rendered_names.push(l(name[uid].name, 'user/' + uid + '/' + game_type + '-' + page_type));
        }
      }
      return rendered_names.join(",<br>");
    }
    else {
      return l(name, 'user/' + uid + '/' + game_type + '-' + page_type);
    }
  }
  catch (error) {console.log('shuffledpack_render_name - ' + error);}
}

/**
 * Renders the top statistics for Spades.
 *
 * @param data
 *   An object containing the top statistics.
 */
function shuffledpack_spades_top_statistics_render(data) {
  var content = "";
  content +=
    t('Player with the most wins') + ': '
    + l(data.most_wins['name'], 'user/' + data.most_wins['uid'] + '/spades-games')
    + ' ' + t("with") + " " + data.most_wins['count']
    + '<br>';
  content +=
    t('Player with the') + ' <a title="' + t("Explanation of Highest Rank") + '" onclick="' + "drupalgap_goto('node/346');" + '">' + t("highest rank") + '</a>:'
    + " " + shuffledpack_render_name(data.highest_rank['name'], data.highest_rank['uid'], "spades", "statistics")
    + " " + t("with") + " " + parseFloat(data.highest_rank['percent']).toFixed(2) + " " + t("percent of")
    + " " + data.highest_rank['games'] + " " + t("games")
    + '<br>';
  content +=
    t('Partership with the most wins:')
    + " " + shuffledpack_render_name(data.partnership_most_wins['name'], null, "spades", "statistics")
    + " " + t("with") + " " + data.partnership_most_wins['count'] + " " + t('wins')
    + '<br>';
  content +=
     t('Partnership with the') + ' <a title="' + t("Explanation of highest rank") + '" onclick="' + "drupalgap_goto('node/346');" + '">' + t("highest rank") + '</a>:'
     + " " + shuffledpack_render_name(data.partnership_highest_rank['name'], null, "spades", "statistics")
     + " " + t("with") + " " + parseFloat(data.partnership_highest_rank['percent']).toFixed(2) + " " + t("percent of")
     + " " + data.partnership_highest_rank['games'] + " " + t("games")
     + '<br>';
  return theme("markup", {markup: content});
}

/**
 * Pulls statistics from the server.
 */
function shuffledpack_statistics_load(options, game_type) {
  try {
    options.method = 'POST';
    options.path = 'shufpack/' + game_type + '-statistics.json';
    options.service = 'shufpack';
    options.resource = game_type + '-statistics';
    Drupal.services.call(options);
  }
  catch (error) {console.log('shuffledpack_statistics_load - ' + error);}
}

/**
 * Pulls statistics about the top players.
 */
function shuffledpack_top_statistics_load(options) {
  try {
    options.method = 'POST';
    options.path = 'shufpack/top-statistics.json';
    options.service = 'shufpack';
    options.resource = 'top-statistics';
    Drupal.services.call(options);
  }
  catch (error) {console.log('shuffledpack_top_statistics_load - ' + error);}
}

/**
 * Implements hook_form_alter().
 *
 * Allow choice of hostname to connect to.
 * Remove title element from games.
 * Hide field_verified on node add form.
 */
function shuffledpack_form_alter(form, form_state, form_id) {
  try {
    if (form_id == 'user_login_form') {
      form.elements['hostname'] = {
        type: 'radios',
        title: t("Hostname"),
        default_value: !empty(Drupal.settings.site_path) ? Drupal.settings.site_path : "http://sy32ekx653pktnls.onion",
        required: true,
        weight: 3,
        options: {
          "http://shuffledpack.brom": "shuffledpack.brom",
          "http://dev.shuffledpack.brom": "dev.shuffledpack.brom",
          "http://shuffledpack.len.brom": "shuffledpack.len.brom",
          "http://sy32ekx653pktnls.onion": "sy32ekx653pktnls.onion",
        },
      }
      // Put the hostname field above the buttons.
      form.elements['name'].weight = 1;
      form.elements['pass'].weight = 2;
      form.elements['submit'].weight = 4;
      form.submit.unshift("shuffledpack_user_login_form_submit");
      // Replace the core login submit handler with our own.
      delete($.inArray('user_login_form_submit', form['submit']));
    }
    else if (form_id == 'node_edit') {
      if (empty(form.elements['nid'].default_value)) {
        // A player can't set his own new game as verified.
        form.elements['field_verified'].access = false;
      }
      if (empty(form.elements['field_player']['und'][0].default_value)) {
        // Set the first player in the game and disable the input.
        form.elements['field_player']['und'][0].default_value = jDrupal.user.uid;
        form.elements['field_player']['und'][0].default_value_label = jDrupal.user.name;
        form.elements['field_player']['und'][0].disabled = true;
      }
      if (form.bundle == 'game') {
        // Games don't need titles.
        form.elements['title'].type = 'hidden';
        form.elements['title'].default_value = t('Completed Game')
        if (form.bundle == 'game') {
          form.elements['field_player']['und'][2].access = false;
          form.elements['field_player']['und'][3].access = false;
          form.elements['field_winner']['und'][1].access = false;
        }
      }
      else if (form.bundle == 'completed_spades_game') {
        // Games don't need titles.
        form.elements['title'].type = 'hidden';
        form.elements['title'].default_value = t('Completed Game')
      }
    }
  }
  catch (error) {console.log("shuffledpack_form_alter - " + error);}
}

/**
 * Submit handler for the user login form.
 *
 * Connect to the selected hostname.
 * Improve error handling.
 */
function shuffledpack_user_login_form_submit(form, form_state) {
  try {
    nositepath_reconnect(form_state.values['hostname']);

    user_login(form_state.values.name, form_state.values.pass, {
      success: function(result) {
        drupalgap_goto(
            typeof form.action !== 'undefined' ?
                form.action : drupalgap.settings.front,
            { reloadPage:true }
        );
      },
      error: function(xhr, status, message) {
        if (status < 200 || status > 499) {
          console.log("shuffledpack: Connection failed.", {xhr: xhr, status: status, message: message});
          alert("Connection failed. After you dismiss this you may see a \"loading\" indicator.  Don't hold your breath.");
        }
      }
    });
  }
  catch (error) {console.log('er_user_login_form_submit - ' + error);}
}

/**
 * Form constructor for deleting a node.
 */
function node_delete_form(form, form_state, node) {
  try {
    // Setup form defaults.
    form.entity_type = 'node';
    form.bundle = node.type;

    // Add the entity's core fields to the form.
    drupalgap_entity_add_core_fields_to_form('node', node.type, form, node);

    form.buttons['delete'] = drupalgap_entity_edit_form_delete_button('node', node.nid);

    return form;
  }
  catch (error) {console.log('node_delete_form - ' + error);}
}

/**
 * Implements hook_block_info().
 */
function shuffledpack_block_info() {
  try {
    var blocks = {};
    blocks['shuffledpack_server_status'] = {
      delta: 'shuffledpack_server_status',
      module: 'shuffledpack',
    };
    return blocks;
  }
  catch (error) {console.log('shuffledpack_block_info - ' + error);}
}

/**
 * Implements hook_block_view().
 */
function shuffledpack_block_view(delta, region) {
  try {
    var content = {};
    if (delta == 'shuffledpack_server_status') {
      content = bl("Server status", "#shuffledpack-ping");
    }
    return content;
  }
  catch (error) {console.log('shuffledpack_block_view - ' + error);}
}

/**
 * Renders a panel for information about the server status.
 */
function shuffledpack_server_status_render() {
  try {
    setTimeout(function() {
      if ($('#shuffledpack-ping').length != 0) {
        return;
      }
      var ping = shuffledpack_init_ping();
      var content = {};
      content['content'] = {
        markup: '<div id="shuffledpack-ping"><p>' + t("Server last pinged:") + ' <span id="shuffledpack-ping-date">' + ping.date + '</span>'
        + '<br>'
        + t("Status") + ': <span id="shuffledpack-ping-status">' + ping.status + '</span><br>' + theme('button', {
          text: t("Ping"),
          attributes: {
            'data-icon': 'cloud',
            'data-role': 'button',
            onclick: "shuffledpack_init_ping(true);",
          },
        })
        + '</p></div>',
      };
      $('body').append(drupalgap_render(content));
      $('#shuffledpack-ping').panel();
      $('#shuffledpack-ping').trigger("create");
    }, 500);
  }
  catch (error) {console.log('shuffledpack_server_status_render - ' + error);}
}

/**
 * Sets or refreshes the date the server was last pinged.
 *
 * @param force
 *   Set to true to skip the timer.
 *
 * @return
 *   An object with the following keys:
 *   - "date": A string of when the server was last pinged.
 *   - "status": A string indicating the result of the ping.
 */
function shuffledpack_init_ping(force) {
  try {
    if (typeof drupalgap.settings.shuffledpack == 'undefined') {
      drupalgap.settings.shuffledpack = {
        ping: {
          date: t("not yet"),
          status: t("unknown"),
        },
      };
    }
    if (typeof drupalgap.settings.site_path != "undefined") {
      if (!force && typeof drupalgap.settings.shuffledpack.ping.date != "string") {
        var now = new Date();
        // Check every 60 seconds or more.
        if (now - drupalgap.settings.shuffledpack.ping.date > 60000) {
          shuffledpack_ping();
        }
      }
      else {
        shuffledpack_ping();
      }
    }
    else {
      return {
        date: t("not yet"),
        status: t("No server selected yet."),
      };
    }
    return drupalgap.settings.shuffledpack.ping;
  }
  catch (error) {console.log('shuffledpack_init_ping - ' + error);}
}

/**
 * Pings the server and sets the date.
 */
function shuffledpack_ping() {
  try {
    var xhr = new XMLHttpRequest();
    var file = drupalgap.settings.site_path + '/robots.txt';
    var random_num = Math.round(Math.random() * 10000);

    xhr.open('HEAD', file + "?rand=" + random_num, true);
    xhr.send();

    xhr.addEventListener("readystatechange", function () {
      if (xhr.readyState == 4) {
        drupalgap.settings.shuffledpack.ping.date = new Date();
        if (xhr.status >= 200 && xhr.status < 304) {
          // Can connect to the server.
          drupalgap.settings.shuffledpack.ping.status = t("success");
        }
        else {
          drupalgap.settings.shuffledpack.ping.status = t("failure");
          console.log('shuffledpack: status was ' + xhr.status);
        }
        $("#shuffledpack-ping-status").text(drupalgap.settings.shuffledpack.ping.status);
        $("#shuffledpack-ping-date").text(drupalgap.settings.shuffledpack.ping.date);
      }
      else {
        console.log(xhr, 'xhr');
      }
    }, false);
  }
  catch (error) {console.log('shuffledpack_ping - ' + error);}
}
